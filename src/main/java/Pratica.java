
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ teste = new PontoXZ(-3,2);
        PontoXY teste2 = new PontoXY(0,2);
        
        System.out.println("Distancia = " + teste.dist(teste2));
        
    }
    
}

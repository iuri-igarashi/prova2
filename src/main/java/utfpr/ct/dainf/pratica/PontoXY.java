/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1100629
 */
public class PontoXY extends Ponto2D{

    public PontoXY() {
        super(0,0,0);
    }

    public PontoXY(double x, double y, double z) {
        super(x, y, 0);
    }
    
    public PontoXY(double x, double y) {
        super(x, y, 0);
    }
    
    @Override
    public String toString(){
        String ponto = getNome() + "XY" + "(" + getX() + "," + getY() + ")";
        return ponto;
    }
    
}
